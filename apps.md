## Table of Contents
- [**Full start**](#full-start)
- [**Full stop**](#full-stop)
- [**Haproxy**](#haproxy)
- [**Lets Encrypt**](#lets-encrypt)
- [**Jellyfin**](#jellyfin)
- [**Minidlna**](#minidlna)
- [**Nzbget**](#nzbget)
- [**Lidarr**](#lidarr)
- [**Radarr**](#radarr)
- [**Sonarr**](#sonarr)
- [**Medusa**](#medusa)
- [**MakeMKV**](#makemkv)
- [**Ffmpeg**](#ffmpeg)
- [**Rtmpdump**](#rtmpdump)
- [**Audio**](#audio)
  - [**abcde**](#abcde-rip-audio-disc)
  - [**Google Music**](#google-music)


## Aliases
```
cat > /root/aliases << EOF
Array=("haproxy" "nzbget" "jellyfin" "lidarr" "radarr" "sonarr")
for i in start stop; do
  ALIASES="alias AAAapps='for daemon in `echo ${Array[@]}`; do for i in enable AAA; do systemctl \${i} podman-\${daemon}; done; done'"
  ALIASES=`echo ${ALIASES} | sed -e "s/AAA/\$i/g"`
  eval ${ALIASES}
done
for i in "${Array[@]}"; do
  ALIAS="alias start${i}='systemctl start podman-${i}'" && eval ${ALIAS}
done
EOF

source /root/aliases
```

## Full start
```
for daemon in haproxy jellyfin nzbget lidarr radarr sonarr; do for i in enable restart; do systemctl $i podman-$daemon; done; done
```


## Full stop
```
for daemon in haproxy jellyfin nzbget lidarr radarr sonarr; do for i in disable stop; do systemctl $i podman-$daemon; done; done
```


### Let's Encrypt
To bootstrap letsencrypt
* [ ]  Ensure internet A record is correct
* * `dig @1.1.1.1 ${DOMAIN} +short A`
* *  See domain registrar is there is an issue
* [ ]  Ensure internal A record is correct
* * `dig @192.168.0.1 ${DOMAIN} +short A`
* * See router if there is an issue
* [ ]  Ensure host-80tcp-haproxy is enabled on router firewall
* [ ]  Ensure http and https is enabled on host firewall
```
DOMAIN="octacube.co"
mkdir -p /mnt/daemons/haproxy/certs /var/log/letsencrypt
cd /mnt/daemons/haproxy
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-haproxy; done
podman run -it --rm --privileged --name certbot --net=host -v /mnt/daemons/haproxy/certs:/etc/letsencrypt -v /var/log/letsencrypt:/var/log/letsencrypt certbot/certbot -d ${DOMAIN} --agree-tos --standalone --renew-by-default certonly
cat certs/live/${DOMAIN}/fullchain.pem certs/live/${DOMAIN}/privkey.pem > certs/${DOMAIN}.pem
systemctl daemon-reload; for i in enable restart; do systemctl $i podman-haproxy; done

echo | openssl s_client -connect ${DOMAIN}:443 -brief
```


## haproxy
```
cat > /etc/systemd/system/podman-haproxy.service << EOF
[Unit]
Description=Podman container - HAproxy
After=network.target
Requires=mnt-daemons.mount

[Service]
Type=simple
TimeoutStartSec=300
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.1.2
ExecStartPre=-/usr/bin/podman rm -f haproxy
ExecStart=/usr/bin/podman run -it --cpuset-cpus 0,1 --memory 2g --name haproxy --hostname haproxy --ip 172.30.0.100 -p 443:4443 -v /mnt/daemons/haproxy:/usr/local/etc/haproxy -v /mnt/daemons/haproxy/run:/run haproxy:latest
ExecStop=-/usr/bin/podman rm -f haproxy
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

mkdir -p /mnt/daemons/haproxy/run
chown -R 666:666 /mnt/daemons/haproxy
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-haproxy; done
systemctl daemon-reload; for i in enable restart; do systemctl $i podman-haproxy; done
```


## jellyFin
```
cat > /etc/systemd/system/podman-jellyfin.service << EOF
[Unit]
Description=Podman container - Jellyfin
After=network.target
Requires=mnt-daemons.mount
ConditionPathIsMountPoint=/mnt/daemons
ConditionPathIsMountPoint=/mnt/ceph

[Service]
Type=simple
WorkingDirectory=/mnt/daemons/jellyfin
TimeoutStartSec=300
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.101
ExecStartPre=-/usr/bin/podman rm -f jellyfin
ExecStart=/usr/bin/podman run --name jellyfin --hostname jellyfin --ip 172.30.0.101 -v /etc/localtime:/etc/localtime:ro -v /mnt/daemons/jellyfin:/config:z -v /mnt/ceph:/media -v /dev/dri:/dev/dri jellyfin/jellyfin:latest
#ExecStart=/usr/bin/podman run --name jellyfin --hostname jellyfin --net=host --privileged -v /etc/localtime:/etc/localtime:ro -v /mnt/daemons/jellyfin:/config:z -v /mnt/ceph:/media -v /dev/dri:/dev/dri jellyfin/jellyfin:latest
ExecStop=-/usr/bin/podman stop jellyfin
ExecStopPost=-/usr/bin/podman rm -f jellyfin
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

mkdir -p /mnt/daemons/jellyfin/run
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-jellyfin; done
systemctl daemon-reload; for i in enable restart; do systemctl $i podman-jellyfin; done
```


## nzbget
```
cat > /etc/systemd/system/podman-nzbget.service << EOF
[Unit]
Description=Podman container - NZBGet
After=network.target
Requires=mnt-daemons.mount
ConditionPathIsMountPoint=/mnt/daemons
ConditionPathIsMountPoint=/mnt/ceph

[Service]
Type=simple
TimeoutStartSec=300
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.102
ExecStartPre=-/usr/bin/podman rm -f nzbget
ExecStart=/usr/bin/podman run --name nzbget --hostname nzbget --ip 172.30.0.102 -e PUID=666 -e PGID=666 -v /mnt/daemons/nzbget/config:/config -v /mnt/daemons/nzbget/downloads:/downloads linuxserver/nzbget:latest
ExecStop=-/usr/bin/podman rm -f nzbget
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

chown -R 666:666 /mnt/daemons/nzbget
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-nzbget; done
systemctl daemon-reload; for i in enable restart; do systemctl $i podman-nzbget; done
```


## lidarr
```
cat > /etc/systemd/system/podman-lidarr.service << EOF
[Unit]
Description=Podman container - lidarr
After=network.target
Requires=mnt-daemons.mount
ConditionPathIsMountPoint=/mnt/daemons
ConditionPathIsMountPoint=/mnt/ceph

[Service]
Type=simple
TimeoutStartSec=300
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.103
ExecStartPre=-/usr/bin/podman rm -f lidarr
ExecStart=/usr/bin/podman run --name lidarr --hostname lidarr --ip 172.30.0.103 -e PUID=666 -e PGID=666 -v /mnt/daemons/lidarr:/config -v /mnt/daemons/nzbget/downloads:/downloads -v /mnt/ceph/Music:/media linuxserver/lidarr:latest
ExecStop=/usr/bin/podman stop lidarr
ExecStopPost=/usr/bin/podman rm -f lidarr
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

mkdir -p /mnt/daemons/lidarr/run
chown -R 666:666 /mnt/daemons/lidarr
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-lidarr; done
systemctl daemon-reload; for i in enable restart; do systemctl $i podman-lidarr; done
```


## radarr
```
cat > /etc/systemd/system/podman-radarr.service << EOF
[Unit]
Description=Podman container - radarr
After=network.target
Requires=mnt-daemons.mount
ConditionPathIsMountPoint=/mnt/daemons
ConditionPathIsMountPoint=/mnt/ceph

[Service]
Type=simple
TimeoutStartSec=300
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.104
ExecStartPre=-/usr/bin/podman rm -f radarr
ExecStart=/usr/bin/podman run --name radarr --hostname radarr --ip 172.30.0.104 -e PUID=666 -e PGID=666 -v /mnt/daemons/radarr:/config -v /mnt/daemons/nzbget/downloads:/downloads -v /mnt/ceph/Movies:/media linuxserver/radarr:latest
ExecStop=/usr/bin/podman stop radarr
ExecStopPost=/usr/bin/podman rm -f radarr
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

mkdir -p /mnt/daemons/radarr/
chown -R 666:666 /mnt/daemons/radarr
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-radarr; done
systemctl daemon-reload; for i in enable restart; do systemctl $i podman-radarr; done
```


## sonarr
```
cat > /etc/systemd/system/podman-sonarr.service << EOF
[Unit]
Description=Podman container - sonarr
After=network.target
Requires=mnt-daemons.mount
ConditionPathIsMountPoint=/mnt/daemons
ConditionPathIsMountPoint=/mnt/ceph

[Service]
Type=simple
TimeoutStartSec=300
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.105
ExecStartPre=-/usr/bin/podman rm -f sonarr
ExecStart=/usr/bin/podman run --name sonarr --hostname sonarr --ip 172.30.0.105 -e PUID=666 -e PGID=666 -v /mnt/daemons/sonarr:/config -v /mnt/daemons/nzbget/downloads:/downloads -v /mnt/ceph/TV:/media linuxserver/sonarr:latest
ExecStop=/usr/bin/podman stop sonarr
ExecStopPost=/usr/bin/podman rm -f sonarr
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

mkdir -p /mnt/daemons/sonarr/run
chown -R 666:666 /mnt/daemons/sonarr
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-sonarr; done
systemctl daemon-reload; for i in enable restart; do systemctl $i podman-sonarr; done
```


## MakeMKV
https://www.makemkv.com/forum2/viewtopic.php?f=3&t=224
https://shkspr.mobi/blog/2012/02/command-line-backup-for-dvds/
```
i='1.87.8'

https://forum.makemkv.com/forum/viewtopic.php?f=5&t=1053
export LIC=""

cd ~
rm -rf makemkv*
wget http://www.makemkv.com/download/makemkv-bin-$i.tar.gz http://www.makemkv.com/download/makemkv-oss-$i.tar.gz
gunzip makemkv*.gz
tar -xf makemkv-oss*.tar; tar -xf makemkv-bin*.tar
cd ~; cd makemkv-oss*; ./configure --disable-gui; make; make install;
cd ~; cd makemkv-bin*; echo "yes" | make install;
echo -n 'appKey = "'$LIC'"' > ~/.MakeMKV/settings.conf
makemkvcon
```


### MKV from disc
```
makemkvcon mkv disc:0 all ./
```


### MKV from iso
```
PATH_TO_ISO="$(pwd)/iso.iso"
TARGET_DIR='.'
makemkvcon mkv iso:"$PATH_TO_ISO" all $TARGET_DIR
```


### MKV from backup
```
cd <into top level dir>; makemkvcon -r --decrypt --directio=true mkv file:./ all <target path>
```


## ffmpeg

### Setup
```
yum install -y ffmpeg
```

### Video

#### Convert to VP9
```
i='/tmp/file.mkv'
ffmpeg -i "${i}" -c:v libvpx-vp9 -c:a libopus "${i}".webm
```

#### Remove subtitles
```
i='/tmp/file.mkv'

ffmpeg -i "${i}" -map 0 -map -0:s -c copy $(basename "${i}")

dd if=$(basename "${i}") of="${i}"
rm -rf $(basename "${i}")
```

#### Remove audio track
```
i='/tmp/file.mkv'
a="<Audio Stream # as found from ffmpeg -i ${i}>"

ffmpeg -i "${i}" -map 0 -map -0:a:0 -c copy $(basename "${i}")

dd if=$(basename "${i}") of="${i}"
rm -rf $(basename "${i}")
```

## youtube-dl
https://youtube-dl.org/

#### Setup
https://ytdl-org.github.io/youtube-dl/download.html

#### Extract video and audio...
```
youtube-dl https://www.youtube.com/watch?v=M6BYljIfdV4
```

#### Extract audio only...
```
youtube-dl --extract-audio --audio-format mp3 https://www.youtube.com/watch?v=ABCDEF12345
```

## rtmpdump
```
yum install -y rtmpdump
```
https://rtmpdump.mplayerhq.hu/rtmpdump.1.html
https://en.wikipedia.org/wiki/RTMPDump

## Audio

### abcde - rip audio disc
https://linux.die.net/man/1/abcde

#### installation
```
yum install -y libdiscid libdiscid-devel
perl -MCPAN -e 'install 'WebService::MusicBrainz'
perl -MCPAN -e 'install MusicBrainz::DiscID'
```

Directions to install here: https://abcde.einval.com/wiki/

#### rip to flac
```
abcde -o flac
```

#### rip to single file flac
```
abcde -1 -o flac
```
