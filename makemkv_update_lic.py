#!/bin/env python

from __future__ import print_function
from bs4 import BeautifulSoup as bs
import cookielib
import os
import urllib2

makemkv_conf_f = os.environ.get('HOME') + '/.MakeMKV/settings.conf'
beta_key_url = 'http://www.makemkv.com/forum2/viewtopic.php?f=5&t=1053'
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}


print('Query ' + beta_key_url + ' for new license key...', end='')

req = urllib2.Request(beta_key_url, headers=hdr)
try:
    for line in bs(urllib2.urlopen(req), 'lxml').get_text().splitlines():
        if 'Code:' in line:
            print('found')
            line = line.split(': Select all')[1].split(' ')[0]
            with open(makemkv_conf_f, 'w') as f:
                f.write('app_Key = "' + line + '"\n')
            print('Update complete!')
except urllib2.URLError as e:
    print(e.reason)