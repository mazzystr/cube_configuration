## Table of Contents
- [Platform](https://gitlab.com/mazzystr/cube_configuration/blob/master/platform.md)
- [Ceph](https://gitlab.com/mazzystr/cube_configuration/blob/master/ceph.md)
- [Apps](https://gitlab.com/mazzystr/cube_configuration/blob/master/apps.md)
