## Table of Contents
- [**Build / Ceph node**](#build-ceph-node)
- [**DNS**](#dns)
- [**Dependencies and tools**](#dependencies-and-tools)  
- [**Performance tuning**](#performance-tuning)  
- [**Firewall**](#Firewall)  
- [**Users**](#users)  
- [**Sensors**](#sensors)  
- [**mDNS**](#mdns)  
- [**40earlyssh**](#40earlyssh)  
- [**CIFS**](#cifs)  
- [**GPG Encryption**](#gpg-encryption)  
- [**OpenSSL Encryption**](#openssl-encryption)
- [**K8S, Containers and such**](#k8s-containers-and-such)
- [**Monitoring and such**](#monitoring-and-such)
- [**Data archiving**](#data-archiving)
  - [**Initialize HashBack**](#initialize-hashBack)  
  - [**Backup files**](#backup-files)  
  - [**Recover files**](#recover-files)  
  - [**Write bluray disc**](#write-bluray-disc)  

## DNS
* Domain: octacube.co
* Registrar: Domain.com

## Build / [Ceph node](https://pcpartpicker.com/user/mazzystr/saved/#view=2MH3bv)  
* Need cables...<br>
  [moddiy](https://www.moddiy.com/products/Seasonic-Premium-Single-Sleeved-4xSATA-Modular-Cable-%28Red%29.html)  
  Or MOLEX cables [amazon](https://www.amazon.com/gp/product/B007OULQ3Y/ref=ox_sc_act_title_1?smid=A3GO5VFCNOM5I7&psc=1)
* Need [USB 3.0 hub](https://www.amazon.com/Anker-9-Port-Smart-Charging-Adapter/dp/B005NGQWL2?SubscriptionId=AKIAINYWQL7SPW7D7JCA&tag=aboutcom02lifewire-20&linkCode=sp1&camp=2025&creative=165953&creativeASIN=B005NGQWL2&ascsubtag=4142295%7Cduckduckgo.com%7C%7C%7C47%2C34%2C51%2C70%2C9%7C1%7C)
* Need 6 x [TCSUNBO 128 Gb MLC](https://www.amazon.com/TCSUNBOW-128GB-Ultrabook-Desktop-2242mm/dp/B078H476CZ/ref=pd_sbs_147_1?_encoding=UTF8&pd_rd_i=B078H476CZ&pd_rd_r=04a887a6-b51e-11e8-a8ea-092258445276&pd_rd_w=jhexl&pd_rd_wg=8rnjc&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=0bb14103-7f67-4c21-9b0b-31f42dc047e7&pf_rd_r=WNM23RAB0X31NB0YP8GS&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=WNM23RAB0X31NB0YP8GS)

## Dependencies and tools
```
rm -rf /etc/profile.d/perl-homedir.csh
yum localinstall -y https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm
yum localinstall -y http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum erase -y perl-homedir
yum groupinstall -y "Development Tools"
yum install -y kernel-devel libcgroup libcgroup-tools avahi-tools rng-tools \
               cryptsetup-luks lvm2 dmraid bash-completion bash-completion-extras \
               gcc git openssl-devel lsof python-pip python-setuptools nmap-ncat \
               hdparm smartmontools usbutils yum-utils atop htop fio net-tools \
               unzip bzip2 strace screen lshw ledctl fio atop htop iotop llvm clang glibc-static-2.17-292.el7.x86_64
yum install -y intel-cmt-cat intel-cmt-cat-devel.x86_64 intel-gpu-tools \
               libva libva-utils libva gstreamer1-vaapi libdrm libdrm-devel \
               libtool libpciaccess libpciaccess-devel ocl-icd ocl-icd-devel opencl-headers
yum install -y centos-release-scl
yum install -y devtoolset-6
pip install gnupg pycrypto python-gnupg simplegist
yum update -y
```

## Performance tuning
```
tuned-adm profile latency-performance
```
```
for i in $(lshw -class disk -class storage -short | grep disk | awk {'print $2'} | sed 's/\/dev\///g' | sort | uniq); do
cat << EOF > /etc/udev/rules.d/98-dev-${i}.rules
ACTION=="add|change", KERNEL=="${i}", ATTR{queue/scheduler}="deadline", ATTR{bdi/read_ahead_kb}="16", ATTR{bdi/nr_requests}="4096"
EOF
done
udevadm control --reload-rules
udevadm trigger
```

## Enable Intel QuickSync
```
cd
wget http://repo.streambuilder.pro/open/binary/sb_sys_analyzer_linux.py
python sb_sys_analyzer_linux.py
```
```
yum erase -y cmake
cd /opt && wget https://cmake.org/files/v3.15/cmake-3.15.4.tar.gz
tar -zxf cmake*.tar.gz && cd cmake*
./bootstrap --prefix=/usr/local
gmake
gmake install
cmake --version
cd; rm -rf /opt/cmake*
```
```
mkdir /opt/intel && cd /opt/intel

git clone git@github.com:intel/gmmlib.git && cd gmmlib
git checkout <tag>
mkdir build; cd build
cmake ..
make -j8
make install
cd /opt/intel

git clone -b release/8.x https://github.com/llvm/llvm-project llvm-project
git clone -b ocl-open-80 https://github.com/intel/opencl-clang llvm-project/llvm/projects/opencl-clang
git clone -b llvm_release_80 https://github.com/KhronosGroup/SPIRV-LLVM-Translator llvm-project/llvm/projects/llvm-spirv
git clone https://github.com/intel/llvm-patches llvm_patches
mv llvm-project/clang llvm-project/llvm/tools/

git clone https://github.com/intel/intel-graphics-compiler igc && cd igc
git checkout <tag>
mkdir build; cd build
cmake ..
make -j`nproc`
make install
cd /opt/intel

git clone https://github.com/intel/compute-runtime neo && cd neo
mkdir build; cd build
cmake ..
make -j`nproc`
make install
cd /opt/intel

wget https://github.com/Intel-Media-SDK/MediaSDK/archive/intel-mediasdk-19.2.1.tar.gz
tar -zxf intel-mediasdk-*.tar.gz && cd MediaSDK-intel-mediasdk-*
mkdir build && cd build
cmake ..
make
make install
```
```

```
## Firewall
```
for i in stop disable; do systemctl $i firewalld; done
```

## Users
```
groupadd -g 666 media
useradd -u 996 -g 666 chris
useradd -u 666 -g 666 media
```

## Sensors
```
yum install -y lm_sensors
sensors-detect
for i in enable restart; do systemctl $i lm_sensors; done

yum install -y libatasmart smartmontools hddtemp
for i in enable restart; do systemctl $i smartd; done
```

## mDNS
Enables ping \<hostname\>.local

https://fedoramagazine.org/find-systems-easily-lan-mdns/

```
yum install -y avahi nss-mdns
for i in enable restart; do systemctl $i avahi-daemon.socket; systemctl $i avahi-daemon.service; done
```


## CIFS
```
yum install -y samba cifs-utils
curl https://raw.githubusercontent.com/mazzystr/Server_configuration/master/smb.conf -o /etc/samba/smb.conf
smbpasswd -a media
for i in disable stop; do systemctl $i nmb.service; systemctl $i smb.service; done
for i in enable restart; do systemctl $i nmb.service; systemctl $i smb.service; done
```

## GPG Encryption

### Create key
gpg --gen-key

### Export a public key into file public.key:
gpg --export -a "User Name" > public.key

### Export a private key
gpg --export-secret-key -a "User Name" > private.key

### Import a public key
gpg --import public.key

### Import a private key
gpg --allow-secret-key-import --import private.key

### Encrypt files
gpg -e -r mazzystr@gmail.com \<path\>/\<file\>

### Unencrypt files
gpg --output \<path\>/\<file\> --decrypt \<path\>/\<file\>.gpg

## OpenSSL Encryption

### Encrypt files
openssl enc -aes-256-cbc -in \<path\>/\<file\> -out \<path\>/\<file\>.enc

### Unencrypt files
openssl enc -d -aes-256-cbc -in \<path\>/\<file\>.enc -out \<path\>/\<file\>  

## K8s, Containers and such

### Installing podman
```
sudo yum install -y golang runc git ostree-devel gpgme-devel device-mapper-devel btrfs-progs-devel libassuan-devel libseccomp-devel automake autoconf gettext-devel libtool libxslt libsemanage-devel bison libcap-devel podman 

echo 10000 > /proc/sys/user/max_user_namespaces
echo media:110000:65536 > /etc/subuid
echo media:110000:65536 > /etc/subgid
```

### Minikube 
* Install minikube : https://minikube.sigs.k8s.io/docs/start/
* Install cri-o : https://cri-o.io/
* minikube start command : `minikube start --driver=none --container-runtime=cri-o --disk-size=50g --memory=14336m`

### Create the dummy container
This is necessary to bootstrap the cni bridge network
```
cat > /etc/systemd/system/podman-busybox.service << EOF
[Unit]
Description=Dummy busybox podman container to bootstrap cni
After=network.target

[Service]
Type=simple
TimeoutStartSec=5m
ExecStartPre=-/usr/bin/podman rm -f busybox
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.2
ExecStart=/usr/bin/podman run -it --name busybox --hostname busybox --ip 172.30.0.2 busybox tail -f /dev/null
ExecStop=-/usr/bin/podman rm -f busybox
ExecStopPost=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.2
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target
EOF

daemon=busybox; systemctl daemon-reload; for i in disable stop; do systemctl $i podman-${daemon}; done
daemon=busybox; systemctl daemon-reload; for i in enable start; do systemctl $i podman-${daemon}; done
```

## Monitoring and such

### Prometheus
```
cat > /etc/systemd/system/podman-prom.service << EOF
[Unit]
Description=Podman container - Prometheus
After=network.target

[Service]
Type=simple
WorkingDirectory=/mnt/daemons/prom
TimeoutStartSec=300
ExecStart=/usr/bin/podman run -rm --name prom --hostname prom --net=host -v /mnt/daemons/prom/prometheus.yml:/etc/prometheus/prometheus.yml -v /mnt/daemons/prom/storage.tsdb.path:/prometheus.tsdb.path/ localhost/cube/prometheus
ExecStop=-/usr/bin/podman stop prom
ExecStopPost=-/usr/bin/podman rm -f prom
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF


mkdir /mnt/daemons/prom /mnt/daemons/prom/storage.tsdb.path
curl blah -o /mnt/daemons/prom/prometheus.yml
chown 65536:65536 /mnt/daemons/prom/*
daemon=prom; systemctl daemon-reload; for i in disable stop; do systemctl $i podman-${daemon}; done
daemon=prom; systemctl daemon-reload; for i in enable start; do systemctl $i podman-${daemon}; done
```

### Prometheus Node Exporter
```
cat > /etc/systemd/system/podman-prom_node_exporter.service << EOF
[Unit]
Description=Podman container - Prometheus Node Exporter
After=network.target

[Service]
Type=simple
WorkingDirectory=/mnt/daemons/prom_node_exporter
TimeoutStartSec=300
ExecStart=/usr/bin/podman run -it -rm --cap-add=SYS_TIME --name prom_node_exporter --hostname prom_node_exporter --net=host -v /:/host:ro,rslave quay.io/prometheus/node-exporter
ExecStop=-/usr/bin/podman stop prom_node_exporter
ExecStopPost=-/usr/bin/podman rm -f prom_node_exporter
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

mkdir -p /mnt/daemons/prom_node_exporter
daemon=prom_node_exporter; systemctl daemon-reload; for i in disable stop; do systemctl $i podman-${daemon}; done
daemon=prom_node_exporter; systemctl daemon-reload; for i in enable start; do systemctl $i podman-${daemon}; done
```

### Prometheus Grafana
```
cat > /etc/systemd/system/podman-prom_grafana.service << EOF
[Unit]
Description=Podman container - Prometheus Grafana
After=network.target

[Service]
Type=simple
WorkingDirectory=/mnt/daemons/prom_grafana
TimeoutStartSec=300
ExecStart=/usr/bin/podman run -rm --name=prom_grafana --hostname prom_grafana --net=host -v /mnt/daemons/prom_grafana/grafana.ini:/etc/grafana/grafana.ini grafana/grafana
ExecStop=-/usr/bin/podman stop prom_grafana
ExecStopPost=-/usr/bin/podman rm -f prom_grafana
Restart=always
RestartSec=30s
StartLimitInterval=60s
StartLimitBurst=99

[Install]
WantedBy=multi-user.target
EOF

mkdir /mnt/daemons/prom_grafana
daemon=prom_grafana; systemctl daemon-reload; for i in disable stop; do systemctl $i podman-${daemon}; done
daemon=prom_grafana; systemctl daemon-reload; for i in enable start; do systemctl $i podman-${daemon}; done
```

## Data archiving
[HashBackup](http://www.hashbackup.com) - Installation procedure [here](http://www.hashbackup.com/home/quickstart)  
[BackBlaze](https://www.backblaze.com) - CLI installation procedure [here](https://www.backblaze.com/b2/docs/quick_command_line.html)

### Initialize HashBack
```
hb init -c /etc/hb -p ask
hb config -c /etc/hb cache-size-limit 0
./backup.py -a restore -t configs
```

### Backup files
```
hb backup -c /etc/hb <path>
```
-c  the backup is stored in the local backup directory specified with the -c option or a default backup directory according to built-in rules if -c isn't used.  If you don't want a local copy of the backup, set cache-size-limit with the config command and create a dest.conf file in the backup directory.  See Destinations for details and examples.

-D  enable dedup, saving only 1 copy of identical data blocks .  -D is followed by the amount of memory you want to dedicate to the  dedup operation, for example:  -D1g would use 1 gigabyte of RAM, -D100m would use 100mb.  The more memory you have available, the more duplicate data HashBackup can detect, though it is still effective even with limited memory.  Also enabled with the dedup-mem config option.  See Dedup Info for detailed information and recommendations.  -D0 disables dedup for this backup.

-v  control how much output is displayed, higher numbers for more:
-v0 = print version, backup directory, copy messages, error count
-v1 = print names of skipped filesystems
-v2 = print names of files backed up (this is the default)
-v3 = print names of excluded files
-X  backup will not cross mount points (separate file systems) unless -X is used, so you have to explicitly list on the command line each filesystem you want to backup.  Use the df command to see a list of your filesystems.  To see what filesystem a directory is on, use df followed by a pathname, or  use df . to see the current directory's filesystem.
  
-t  tag the backup with information-only text shown with hb versions


### Recover files
```
hb recover -c /etc/hb
hb get -c /etc/hb <path>
```
The recover command will fetch all backup files from the remote destination that do not exist in the local backup directory and copy them to the local backup directory.  This copy also occurs if the local file exists but is a different size than the remote file.

To do a full recover, create an empty backup directory, copy the original backup's key.conf and dest.conf files there, then run the recover command.  You do not run the init command before recover.
  
The --force option causes recovery to start without a confirmation prompt.

The -n option skips downloading of any archive files.  Normally archive files are recovered if the cache-size-limit config option is -1 (the default), but this may be a lot of data.  In a disaster recovery situation, only some critical files may need to be restored immediately.  By using -n,  the local backup directory can be recovered quickly, the hb get command can be used to restore the critical files (downloading only the required archives), then the recover command without -n can be used later to recover all archive files.

The -a option downloads archive files even if they already exist in the local backup directory.  This can be used if the local copies are suspect for some reason.

### Write bluray disc
```
DIR=`pwd`
ISO=/tmp/data.iso
mkisofs -r -l -f -allow-limited-size -joliet-long -o "${ISO}" -J "${DIR}"
growisofs -speed=4 -dvd-compat -Z /dev/cdrom=${ISO}
```
or
```
DIR=`pwd`
TAR=/tmp/data.tar
growisofs -speed=4 -dvd-compat -Z /dev/cdrom -R -J "${TAR}"
```
