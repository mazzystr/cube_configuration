#!/usr/bin/env python

"""Script to ...
      make flac backups of audio dics
      apply cddb metadata
      move to filesystem that is protected by RAID
      and served by Plex
"""

from __future__ import print_function
from subprocess import call
from subprocess import check_output
import argparse
import os
import shutil
import CDDB
import DiscID


PARSER = argparse.ArgumentParser()
PARSER.add_argument(
    '-a', '--action', metavar='', default='rip', required=False,
    help='Specify action (options: [rip], eject, tag)')
PARSER.add_argument(
    '--tr', metavar='', default='', required=False,
    help='rip all or track range \
          (options: [all] 1:2, single)')
PARSER.add_argument(
    '--dir', metavar='', default='/mnt/ceph/Music/', required=False,
    help='Update audio file directory as needed \
          (options: [/mnt/ceph/Music/] /dir/to/my/files)')
PARSER.add_argument(
    '--tmpdir', metavar='', default='/mnt/daemons/tmp/', required=False,
    help='Update tmp directory as needed files \
          (options: [/mnt/daemons/tmp/] /tmp)')
PARSER.add_argument(
    '--remove_tags', metavar='', default=False, required=False,
    help='Specify True/False to remove existing tags (options: True, [False])')
ARGS = PARSER.parse_args()
ARGS.dir = ARGS.dir.rstrip('/')


def tmpdir_setup(track_info):
    """Set up tmp directory"""
    for i in track_info.keys():
        try:
            os.stat(ARGS.tmpdir + '/' + i.replace(' ', '_'))
        except OSError:
            os.mkdir(ARGS.dir + '/' + i.replace(' ', '_'))
            os.chmod(ARGS.tmpdir + '/' + i.replace(' ', '_'), 0775)
            os.chown(ARGS.tmpdir + '/' + i.replace(' ', '_'), 666, 666)
        for j in track_info[i]:
            try:
                os.stat(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'))
            except OSError:
                os.mkdir(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'))
                os.chmod(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'), \
                         0775)
                os.chown(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'), \
                         666, 666)


def disc_proc():
    """
    Query cdrom disc
    Return var track_info and disc artist/album name
    """
    track_info = {}
    cdrom = DiscID.open()
    disc_id = DiscID.disc_id(cdrom)
    query = CDDB.query(disc_id)
    if isinstance(query[1], dict):
        info = CDDB.read(query[1]['category'], query[1]['disc_id'])
        title = query[1]['title'].split(' / ')
    else:
        info = CDDB.read(query[1][0]['category'], query[1][0]['disc_id'])
        title = query[1][0]['title'].split(' / ')
    title[1] = title[1].replace('/', '_')
    track_info[title[0]] = {}
    track_info[title[0]][title[1]] = {}
    track_info[title[0]][title[1]]['TrackNum'] = len( \
                                                     [i for i in info[1].keys() \
                                                      if i.startswith('TTITLE') \
                                                     ])
    track_info[title[0]][title[1]]['Tracks'] = []
    track_info[title[0]][title[1]]['Year'] = info[1]['DYEAR']
    track_info[title[0]][title[1]]['Discnum'] = str(1)
    for i in info[1]:
        if 'TTITLE' in i:
            name = info[1][i]
            num = "%.02d" % (int(i.split('TTITLE')[1]) + 1)
            track_info[title[0]][title[1]]['Tracks'].append( \
                                                            num + \
                                                            '-' + \
                                                            name.replace(' ', '_') + \
                                                            '.flac' \
                                                           )
    return track_info


def rip_disc(track_info):
    """Convert audio tracks to wav files"""
    for i in track_info.keys():
        for j in track_info[i].keys():
            os.chdir(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'))
            args = (' -D /dev/cdrom -vall speed=16 cddb=0')
            if not ARGS.tr:
                args = (args + ' -B')
            if ':' in ARGS.tr:
                args = (args + ' -B -t' + ARGS.tr.replace(':', '+'))
            call(["cdda2wav" +  args], shell=True)


def encode_files(track_info):
    """Encode wav files into flac and remove old wav files"""
    print('')
    for i in track_info.keys():
        for j in track_info[i].keys():
            os.chdir(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'))
            for name in os.listdir(ARGS.tmpdir + \
                                   '/' + \
                                   i.replace(' ', '_') + \
                                   '/' + \
                                   j.replace(' ', '_')):
                if '.wav' in name:
                    call(["flac", "-V", "-8", "--delete-input-file", name])


def rename_files(track_info):
    """Rename flac files with cddb metadata"""
    for i in track_info:
        for j in track_info[i]:
            os.chdir(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'))
            flistrm = []
            for _root, _dirs, files in os.walk('.', topdown=True):
                for fname in files:
                    if '.flac' not in fname:
                        flistrm.append(fname)
            for fname in track_info[i][j]['Tracks']:
                try:
                    os.stat(fname)
                except OSError:
                    try:
                        shutil.move('audio' + \
                                    '_' + \
                                    fname.split('-')[0] + \
                                    '.flac', \
                                    fname)
                        os.chown(fname, 666, 666)
                        os.chmod(fname, 0775)
                    except OSError as err:
                        print('Failed to move/chown/chmod file')
                        print(err)
                        print('Investigate problem with filesystem and/or permissions')
                        print('source: %s' % ('audio' + '_' + fname.split('-')[0] + '.flac'))
                        print('target: %s' % (fname))
                        print('Working dir: %s' % (ARGS.tmpdir + \
                                                   '/' + \
                                                   i.replace(' ', '_') + \
                                                   '/' + \
                                                   j.replace(' ', '_') + \
                                                   '/'))
                    except IOError as err:
                        pass
            for xyz in flistrm:
                os.remove(xyz)


def move_tmpdir(track_info):
    """Move tmp directory to final position"""
    os.chdir('/root')
    for i in track_info.keys():
        for j in track_info[i].keys():
            try:
                os.stat(ARGS.dir)
            except OSError:
                print('Really?  td doesn\'t exist!')
                print('Values: %s' % (ARGS.dir))
            try:
                os.stat(ARGS.dir + '/' + i.replace(' ', '_'))
            except OSError:
                os.mkdir(ARGS.dir + '/' + i.replace(' ', '_'))
                os.chmod(ARGS.dir + '/' + i.replace(' ', '_'), 0775)
                os.chown(ARGS.dir + '/' + i.replace(' ', '_'), 666, 666)
            try:
                os.stat(ARGS.dir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'))
                shutil.rmtree(ARGS.dir + \
                              '/' + \
                              i.replace(' ', '_') + \
                              '/' + \
                              j.replace(' ', '_'), \
                              ignore_errors=True)
            except OSError:
                pass
            shutil.move(ARGS.tmpdir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'), \
                        ARGS.dir + '/' + i.replace(' ', '_') + '/' + j.replace(' ', '_'))


def find_artist_album_from_dir(directory):
    """Find artist, album, tracknum, tracktot, trackname \
       based upon filesystem hierarchy"""
    track_info = {}
    flist = []
    for root, _, files in os.walk(directory, topdown=True):
        for fname in files:
            flist.append(os.path.join(root, fname))
    for i in flist:
        a_types = ['.flac', '.m4a', '.mp3']
        if any(xyz in i for xyz in a_types):
            (artalb, track) = i.rsplit('/', 1)
            (_, artist, album) = artalb.rsplit('/', 2)
            if artist not in track_info.keys():
                track_info[artist] = {}
            if album not in track_info[artist].keys():
                track_info[artist][album] = {}
                track_info[artist][album]['Tracks'] = []
                track_info[artist][album]['Year'] = ''
                track_info[artist][album]['disc'] = '1'
            if track not in track_info[artist][album]['Tracks']:
                track_info[artist][album]['Tracks'].append(track)
    for key in track_info[artist].keys():
        track_info[artist][key]['TrackNum'] = len(track_info[artist][key]['Tracks'])
    return track_info


def tag_files(directory, track_info):
    """Tag flac files with cddb metadata"""
    print('')
    for i in track_info.keys():
        for j in track_info[i].keys():
            if j in directory:
                directory = directory[:-(len(j))].rstrip('/')
            if i in directory:
                directory = directory[:-(len(i))].rstrip('/')
            os.chdir(directory + \
                     '/' + \
                     i.replace(' ', '_') + \
                     '/' + \
                     j.replace(' ', '_') + \
                     '/')
            for _, _, files in os.walk('.', topdown=True):
                for ffile in files:
                    args = [\
                    directory, \
                    i.replace('_', ' '), \
                    j.replace('_', ' '), \
                    i.replace('_', ' '), \
                    track_info[i][j]['Year'], \
                    str(ffile[2:4]) + '/' + \
                    track_info[i][j]['Discnum'], \
                    #str(
                    #    len(
                    #        filter(
                    #            lambda s:
                    #            s.startswith(str(ffile[:1])),
                    #            track_info[i][j]['Tracks']
                    #        )
                    #    )
                    #), \
                    str(ffile[:1]), \
                    ffile[5:].replace('.flac', '').replace('_', ' '), \
                    ffile \
                    ]
                    if '.flac' in ffile:
                        tag_flac(args)
                    if 'm4a' in ffile:
                        tag_m4a(args)
                    if '.mp3' in ffile:
                        tag_mp3(args)


def tag_flac(args):
    """Tag the flac files"""
    f_args = {}
    f_args['artist'] = "--set-tag=artist=%s" % (args[1])
    f_args['album'] = "--set-tag=album=%s" % (args[2])
    f_args['albumartist'] = "--set-tag=albumartist=%s" % (args[3])
    f_args['DATE'] = "--set-tag=DATE=%s" % (args[4])
    f_args['TRACKNUMBER'] = "--set-tag=TRACKNUMBER=%s" % (args[5])
    f_args['DISCNUMBER'] = "--set-tag=DISCNUMBER=%s" % (args[6])
    f_args['TITLE'] = "--set-tag=TITLE=%s" % (args[7])
    for i in ('artist', 'album', 'albumartist', 'DATE', 'TRACKNUMBER', 'DISCNUMBER', 'TITLE'):
        if ARGS.remove_tags:
            call(["metaflac", "--remove-tag=" + i, args[8]])
        tagged = check_output(["metaflac", "--show-tag=" + i, args[8]])
        if tagged == '':
            call(["metaflac", f_args[i], args[8]])
    print(args[0] + \
          '/' + \
          args[1].replace(' ', '_') + \
          '/' + \
          args[2].replace(' ', '_') + \
          '/' + \
          args[8])
    call(["metaflac", "--list", "--block-number=2", args[8]])
    print()


def tag_m4a(args):
    """Tag the m4a files"""
    f_args = {}
    f_args['artist'] = "--artist=%s" % (args[1])
    f_args['album'] = "--album=%s" % (args[2])
    f_args['albumArtist'] = "--albumArtist=%s" % (args[3])
    f_args['year'] = "--year=%s" % (args[4])
    f_args['tracknum'] = "--tracknum=%s" % (args[5])
    f_args['disk'] = "--disk=%s" % (args[6])
    f_args['title'] = "--title=%s" % (args[7])
    call(["AtomicParsley", \
          args[8], \
          '--overWrite', \
          f_args['artist'], \
          f_args['album'], \
          f_args['albumArtist'], \
          f_args['year'], \
          f_args['tracknum'], \
          f_args['tracknum'], \
          f_args['title'] \
        ])
    print(args[0] + \
          '/' + \
          args[1].replace(' ', '_') + \
          '/' + \
          args[2].replace(' ', '_') + \
          '/' + \
          args[8])
    print()


def tag_mp3(args):
    """Tag the mp3 files"""
    f_args = {}
    f_args['TPE2'] = "%s" % (args[1])
    f_args['TOAL'] = "%s" % (args[2])
    f_args['TALB'] = "%s" % (args[2])
    f_args['TORY'] = "%s" % (args[4])
    f_args['TRCK'] = "%s" % (args[5])
    f_args['TPOS'] = "%s" % (args[6])
    f_args['TIT2'] = "%s" % (args[7])
    for i in ('TPE2', 'TOAL', 'TALB', 'TORY', 'TRCK', 'TPOS', 'TIT2'):
        if ARGS.remove_tags:
            call(["id3v2", "-r", i, args[8]])
        #tagged = check_output(["id3v2", "-l", args[8] "| grep", i])
        #if tagged == '':
        call(["id3v2", '--' + i, f_args[i], args[8]])
    print(args[0] + \
          '/' + \
          args[1].replace(' ', '_') + \
          '/' + \
          args[2].replace(' ', '_') + \
          '/' + \
          args[8])
    call(["id3v2", "-l", args[8]])
    print()


def eject_disc():
    """All done!  Lets eject this disc!"""
    call(['eject'])


if __name__ == '__main__':
    ABC = ['rip']
    if any(xyz in ARGS.action for xyz in ABC):
        (TRACK_INFO) = disc_proc()
    ABC = ['rip']
    if any(xyz in ARGS.action for xyz in ABC):
        tmpdir_setup(TRACK_INFO)
    ABC = ['rip']
    if any(xyz in ARGS.action for xyz in ABC):
        rip_disc(TRACK_INFO)
    ABC = ['rip', 'eject']
    if any(xyz in ARGS.action for xyz in ABC):
        eject_disc()
    ABC = ['rip']
    if any(xyz in ARGS.action for xyz in ABC):
        encode_files(TRACK_INFO)
    ABC = ['rip']
    if any(xyz in ARGS.action for xyz in ABC):
        rename_files(TRACK_INFO)
    ABC = ['rip']
    if any(xyz in ARGS.action for xyz in ABC):
        move_tmpdir(TRACK_INFO)
    ABC = ['tag']
    if any(xyz in ARGS.action for xyz in ABC):
        (TRACK_INFO) = find_artist_album_from_dir(ARGS.dir)
    ABC = ['rip', 'tag']
    if any(xyz in ARGS.action for xyz in ABC):
        tag_files(ARGS.dir, TRACK_INFO)
