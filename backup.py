#!/usr/bin/env python

from Crypto.Cipher import AES
from Crypto.Hash import HMAC
from Crypto.Protocol.KDF import PBKDF2
from base64 import b64encode, b64decode
from datetime import datetime
from os import mkdir, path, remove
from shutil import rmtree
from socket import gethostname
#import simplegist
import argparse
import getpass
import gnupg
import random
import tarfile
import zlib


gitlabtoken = ''
gitlabuser = 'mazzystr'
filesconfigs = [
    '/etc/ceph',
    '/etc/samba',
    '/etc/hb/cacerts.crt',
    '/etc/hb/dest.conf',
    '/etc/hb/dest.db',
    '/etc/hb/hash.db',
    '/etc/hb/hb.db',
    '/etc/hb/hb.lock',
    '/etc/hb/inex.conf',
    '/etc/hb/key.conf',
    '/mnt/daemons/nzbget/nzbget.conf',
    '/mnt/daemons/sickrage/config.ini',
    '/mnt/daemons/couchpotatodata/settings.conf',
    '/mnt/daemons/headphones/config.ini',
    ]
fileskeys = [
    path.expanduser('~') + '/.ssh'
    ]
action_choices = ['configs', 'keys']

all_action_choices = action_choices + ['all']

parser = argparse.ArgumentParser()

parser.add_argument(
    '-a', '--action', metavar='Action', default='backup', required=True,
    choices = ['backup', 'restore'],
    help='Specify action (options: [backup], restore)')
parser.add_argument(
    '-t', '--type', metavar='Action type', default="configs,keys",
    choices = all_action_choices,
    help='Specify action type (options: [all], configs, keys)')
parser.add_argument(
    '-p', '--pw', metavar='Password', default=None,
    help='Specify password for encrypted file (options: [None])')
args = parser.parse_args()


def b64conv(type):
    if args.action == 'backup':
        print('...Base64 encoding %s' % (type))
        r = open('/tmp/backup_' + type + '.tar.gz.enc', 'rb')
        w = open('/tmp/backup_' + type + '.tar.gz.enc.b64', 'wb')
        w.write(b64encode(r.read()))
    if args.action == 'restore':
        print('...Base64 decoding %s' % (type))
        r = open('/tmp/backup_' + type + '.tar.gz.enc.b64', 'rb')
        w = open('/tmp/backup_' + type + '.tar.gz.enc', 'wb')
        w.write(b64decode(r.read()))


def cleanup(type):
    print('...Cleaning up %s' % (type))
    a = 'tar.gz.enc.b64'
    a = a.split('.')
    for i in xrange(len(a)):
        remove('/tmp/backup_' + type + '.' + '.'.join(a[:i+1]))
    if 'keys' in type:
        remove(path.expanduser('~') + '/.ssh/mykeyfile.asc')
        remove(path.expanduser('~') + '/.ssh/mykeyfilereport.txt')


def compression(type):
    if args.action == 'backup':
        print('...Compressing %s' % (type))
        in_file = open('/tmp/backup_' + type + '.tar', 'rb').read()
        out_file = '/tmp/backup_' + type + '.tar.gz'
        outdata = zlib.compress(in_file, zlib.Z_BEST_COMPRESSION)
    if args.action == 'restore':
        print('...UNCompressing %s' % (type))
        in_file = open('/tmp/backup_' + type + '.tar.gz', 'rb').read()
        outdata = zlib.decompress(in_file)
        out_file = '/tmp/backup_' + type + '.tar'
    out_file = open(out_file, 'wb')
    out_file.write(outdata)


# https://eli.thegreenplace.net/2010/06/25/aes-encryption-of-files-in-python-with-pycrypto
# encrypt: encrypt openssl aes-256-cbc -d \
#             -in /tmp/backupconfigs.tar.gz.enc \
#             -out /tmp/backupconfigs.tar.gz
# decrypt: encrypt openssl aes-256-cbc -d \
#              -in /tmp/backupconfigs.tar.gz.enc \
#              -out /tmp/backupconfigs.tar.gz
def crypt(type):
    if args.action == 'backup':
        print('...Encrypting %s' % (type))
        r = open('/tmp/backup_' + type + '.tar.gz', 'rb')
        w = open('/tmp/backup_' + type + '.tar.gz.enc', 'wb')
        salt = str(path.getsize('/tmp/backup_' + type + '.tar.gz'))
        while len(salt) < 16:
            salt += chr(random.randint(0, 0xFF))
        iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
        kdf = PBKDF2(args.pw, salt, 64, 1000)
        key = kdf[:32]
        key_mac = kdf[32:]
        mac = HMAC.new(key_mac)
        w.write(salt)
        w.write(mac.hexdigest())
        w.write(iv)
        encryptor = AES.new(key, AES.MODE_CBC, iv)
        while True:
            chunk = r.read(256)
            if len(chunk) == 0:
                break
            elif len(chunk) % 16 != 0:
                chunk += ' ' * (16 - len(chunk) % 16)
            w.write(encryptor.encrypt(chunk))
    if args.action == 'restore':
        print('...UNEncrypting %s' % (type))
        r = open('/tmp/backup_' + type + '.tar.gz.enc', 'rb').read()
        w = open('/tmp/backup_' + type + '.tar.gz', 'w')
        salt = r[0:16]
        verify = r[16:48]
        kdf = PBKDF2(args.pw, salt, 64, 1000)
        key = kdf[:32]
        key_mac = kdf[32:]
        mac = HMAC.new(key_mac)
        mac.update(r[32:])
        iv = r[48:64]

        try:
            if not mac.hexdigest() != verify:
                raise OSError("File not found")
        except ValueError:
            print("Oops!  That was no valid number.  Try again...")

        decryptor = AES.new(key, AES.MODE_CBC, iv)
        decrypted = decryptor.decrypt(r[64:])
        w.write(decrypted)


#def githubgist(type):
#    GHgist = simplegist.Simplegist(username=githubuser, api_token=githubtoken)
#    latest = 'backup-' + gethostname() + '-' + type + '-latest'
#    id = GHgist.profile().getMyID(latest)
#    if args.action == 'backup':
#        print('...Creating GitHub gist for %s' % (type))
#
#        if id:
#            b64msgold = GHgist.profile().content(id=id)
#            GHgist.profile().delete(id=id)
#            name = 'backup-' + \
#                   gethostname() + \
#                   '-' + \
#                   type + \
#                   '-' + \
#                   datetime.now().strftime('%Y-%m-%d-%H:%M')
#            GHgist.create(name=name,
#                          description=name,
#                          public=False,
#                          content=b64msgold)
#        with open('/tmp/backup_' + type + '.tar.gz.enc.b64', 'r') as f:
#            b64msg = f.read().replace('\n', '')
#        GHgist.create(name=latest,
#                      description=latest,
#                      public=False,
#                      content=b64msg)
#    if args.action == 'restore':
#        print('...Retrieving GitHub gist %s' % (type))
#        if id:
#            b64msg = GHgist.search('').content(id=id)
#            with open('/tmp/backup_' + type + '.tar.gz.enc.b64', 'wb') as f:
#                f.write(b64msg)
#            f.close()
#        else:
#            print('Sorry ... there are no restore options')


def gnupg_keys():
    gpg = gnupg.GPG(homedir=path.expanduser('~') + '/.gnupg')
    gpg.encoding = 'utf-8'
    if args.action == 'backup':
        print("...Exporting %s" % (gnupg_keys.__name__))
        public_keys = gpg.list_keys()
        private_keys = gpg.list_keys(True)
        with open(path.expanduser('~') + '/.ssh/mykeyfile.asc', 'w+') as f:
            f.write(gpg.export_keys(args.pw))
            f.write(gpg.export_keys(args.pw, True))
        with open(path.expanduser('~') +
                  '/.ssh/mykeyfilereport.txt', 'w') as f:
            f.write('%spublic keys: %s' % ("\t", public_keys))
            f.write('%sprivate keys: %s' % ("\t", private_keys))
    if args.action == 'restore':
        print("...Restoring %s" % (gnupg_keys.__name__))
        try:
            print(path.expanduser('~') + '/.gnupg')
            rmtree(path.expanduser('~') + '/.gnupg')
        except OSError:
            pass
        mkdir(path.expanduser('~') + '/.gnupg', 0o700)
        key_data = open(path.expanduser('~') + '/.ssh/mykeyfile.asc').read()
        import_result = gpg.import_keys(key_data)
        print(import_result.results)


def tar(type):
    if args.action == 'backup':
        print('...Tarring %s' % (type))
        f = tarfile.open('/tmp/backup_' + type + '.tar', 'w')
        for j in globals()['files' + type]:
            f.add(j)
        f.close()
    if args.action == 'restore':
        print('...UNTarring %s' % (type))
        f = tarfile.open('/tmp/backup_' + type + '.tar', 'r')
        f.extractall(path="/")
        f.close()


if __name__ == '__main__':
    if not args.pw:
        while not args.pw:
            p1 = getpass.unix_getpass(
                prompt='Enter a password: ',
                stream=None)
            if args.action == 'backup':
                p2 = getpass.unix_getpass(
                    prompt='Confirm password: ',
                    stream=None)
                if p1 == p2:
                    args.pw = p1
            if args.action == 'restore':
                args.pw = p1
    print('Starting %s ... ' % (args.action))
    args.type = action_choices if 'all' in args.type else [ args.type ]
    for i in args.type:
        if args.action == 'backup':
            #gnupg_keys() if 'keys' in i else None
            tar(i)
            compression(i)
            crypt(i)
            b64conv(i)
            #gitlabsnippet(i)
            #cleanup(i)
        if args.action == 'restore':
            githubgist(i)
            b64conv(i)
            crypt(i)
            compression(i)
            tar(i)
            gnupg_keys() if 'keys' in i else None
            cleanup(i)
    print('Done!')