# Ceph

## Table of Contents
- [**Getting started**](#getting-started)  
  - [**Install**](#install)  
  - [**Create configuration file**](#create-configuration-file)  
  - [**Create mons instance**](#create-mons-instance)
  - [**Create osd instance**](#create-osd-instance)
  - [**Modify Crush Map**](#modify-crush-map)
  - [**Create RADOS pools**](#create-rados-pools)
  - [**Create filesystem**](#create-filesystem)  
  - [**Create mgr instance**](#create-mgr-instance)
- [**Operations**](#operations)  
  - [**Monitoring**](#monitoring)  
  - [**Backup configuration**](#backup-configuration)  
  - [**Restore configuration**](#restore-configuration)  
  - [**Start cluster**](#start-ceph-cluster)
  - [**Stop cluster**](#stop-ceph-cluster)
  - [**Remove osd**](#remove-ceph-osd-failed-disk)  
  - [**Disaster**](#disaster)  

## Getting started

### Install
```
yum localinstall -y https://download.ceph.com/rpm-mimic/el7/noarch/ceph-release-1-1.el7.noarch.rpm
yum install -y ceph libcgroup-tools
pip install --upgrade pip
pip install boto boto3
pip install --upgrade setuptools
pip install --upgrade python-swiftclient
```

### Create configuration file

#### Restore configuration
```
curl https://raw.githubusercontent.com/mazzystr/Server_configuration/master/ceph.conf -o /etc/ceph/ceph.conf
chown ceph:ceph /etc/ceph/ceph.conf
FSID=$((cat /etc/ceph/ceph.conf 2> /dev/null | grep fsid || uuidgen) | sed s/"  fsid = "//g)
```

#### Create new configuration
Set Ceph cluster id
```
FSID=$((cat /etc/ceph/ceph.conf 2> /dev/null | grep fsid || uuidgen) | sed s/"  fsid = "//g)
```
Create Ceph configuration file
```
cat << EOF > /etc/ceph/ceph.conf
[global]
  fsid = ${FSID}
  mon initial members = bridge
  mon host = 172.30.0.1
  auth_cluster_required = none
  auth_service_required = none
  auth_client_required = none
  osd pool default size = 3
  osd pool default min size = 2
  osd crush chooseleaf type = 0
  enable experimental unrecoverable data corrupting features = bluestore rocksdb
  osd objectstore = bluestore
  osd recovery sleep hdd = 0
  osd recovery sleep hybrid = 0
  osd deep scrub interval = 1209600

[mon]
  mon_osd_nearfull_ratio = 0.95
  mon_osd_backfillfull_ratio = 0.97
  mon_osd_full_ratio = 0.97
  mon pg warn max per osd = 0

[mon.bridge]
  host = bridge
  mon addr = 172.16.0.10:6789

[mds]

[osd]
  cluster network = 172.30.0.0/16
  bluestore min alloc size = 4096
  bluestore min alloc size ssd = 4096
  bluestore min alloc size hdd = 4096
  osd memory target = 1610612736

EOF
```

### Create mons instance
```
FSID=$(cat /etc/ceph/ceph.conf | grep fsid | awk {'print $3'})
MON_HOST='mon1'

cat << EOF > /etc/ceph/ceph.conf
[mon.${MON_HOST}]
  host = ${MON_HOST}
  mon addr = <MON_HOST IP NEEDED>:6789

EOF

ceph-authtool /etc/ceph/keyring.mon.${MON_HOST} --create-keyring --gen-key -n mon. --cap mon 'allow *'
ceph-authtool /etc/ceph/keyring.mon.${MON_HOST} -p -n mon.
ceph-authtool --create-keyring /etc/ceph/keyring.client.admin --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow'
ceph-authtool /etc/ceph/keyring.mon.${MON_HOST} --import-keyring /etc/ceph/keyring.client.admin
chown ceph:ceph /etc/ceph/keyring*

ceph-mon --conf /etc/ceph/ceph.conf \
         --setuser ceph \
         --setgroup ceph \
         --cluster ceph \
         --mkfs -i ${MON_HOST} \
         --fsid ${FSID} --keyring /etc/ceph/keyring.mon.${MON_HOST}

cat > /etc/systemd/system/podman-ceph_${MON_HOST}.service << EOF
[Unit]
Description=Podman container - Ceph mon - ${MON_HOST}
After=network.target

[Service]
Environment=VER=v19.2.0-20240927
Type=simple
WorkingDirectory=/var/lib/ceph/mon/ceph-${MON_HOST}
TimeoutStartSec=300
ExecStartPre=-/usr/bin/podman rm -f ceph_${MON_HOST}
ExecStart=/usr/bin/podman run --privileged --pid=host --cpuset-cpus 0,1 --memory 2g --name ceph_${MON_HOST} --hostname ceph_${MON_HOST} --net ceph:ip=172.30.1.2 -v /dev:/dev -v /etc/localtime:/etc/localtime:ro -v /etc/ceph:/etc/ceph/ -v /var/lib/ceph/mon/ceph-mon1:/var/lib/ceph/mon/ceph-${MON_HOST} -v /var/log/ceph:/var/log/ceph -v /run/udev/:/run/udev/ ceph/ceph:${VER} ceph-mon -f --cluster ceph --id ${MON_HOST} --setuser ceph --setgroup ceph
ExecStop=-/usr/bin/podman rm -f ceph_${MON_HOST}
Restart=always
RestartSec=10s
TimeoutStartSec=120
TimeoutStopSec=15

[Install]
WantedBy=multi-user.target
EOF
```

MON CONTAINER COMMANDS NEEDED


### Create osd instance
Initialize physical disk
```
### RUN ONCE PER HOST ###
DEVICE_TYPE=nvme0n; DEVICE_ID=1; DEVICE_PART=p
parted /dev/${DEVICE_TYPE}${DEVICE_ID} -s -- mklabel gpt
parted /dev/${DEVICE_TYPE}${DEVICE_ID} -s unit GB mkpart nvme 0 100%
partprobe
pvcreate /dev/${DEVICE_TYPE}${DEVICE_ID}${DEVICE_PART}1
vgcreate vg /dev/${DEVICE_TYPE}${DEVICE_ID}${DEVICE_PART}1
### END ###


OSD_ID=$(ceph osd create)
luksKey='blah'

lvcreate -L 1G -n ceph-${OSD_ID} vg
lvcreate -L 1G -n ceph-${OSD_ID}wal vg
lvcreate -L 50G -n ceph-${OSD_ID}db vg

for i in "" db wal; do 
  printf ${luksKey} | cryptsetup --force-password luksFormat /dev/mapper/vg-ceph--${OSD_ID}${i}
  printf ${luksKey} | cryptsetup --force-password open /dev/mapper/vg-ceph--${OSD_ID}${i} ceph-${OSD_ID}${i}
done

mkfs.xfs -f -b size=4096 -s size=4096 /dev/mapper/ceph-${OSD_ID}
xfs_admin -L ceph-${OSD_ID} /dev/mapper/ceph-${OSD_ID}
cat << EOF >> /etc/fstab
/dev/mapper/ceph-${OSD_ID} /var/lib/ceph/osd/ceph-${OSD_ID} xfs rw,noexec,nodev,noatime,nodiratime,inode64,logbufs=8 0 0
EOF
if [ "$(stat -c %F /var/lib/ceph/osd/ceph-${OSD_ID} 2> /dev/null)" != "directory" ]; then
  mkdir /var/lib/ceph/osd/ceph-${OSD_ID};
fi
mount /var/lib/ceph/osd/ceph-${OSD_ID}

cat >> /etc/crypttab << EOF
ceph-${OSD_ID} UUID=$(blkid /dev/mapper/vg-ceph--${OSD_ID} | awk -F"\"" {'print $2'}) none
ceph-${OSD_ID}db UUID=$(blkid /dev/mapper/vg-ceph--${OSD_ID}db | awk -F"\"" {'print $2'}) none
ceph-${OSD_ID}wal UUID=$(blkid /dev/mapper/vg-ceph--${OSD_ID}wal | awk -F"\"" {'print $2'}) none
EOF

LETTER=b
export DEVICE_TYPE=sd; export DEVICE_ID=${LETTER:-b}; export DEVICE_PART=''

echo yes | parted /dev/${DEVICE_TYPE}${DEVICE_ID} mklabel gpt
parted /dev/${DEVICE_TYPE}${DEVICE_ID} -s unit GB mkpart ceph-${OSD_ID}-block 0 100%
partprobe

printf ${luksKey} | cryptsetup --force-password luksFormat /dev/${DEVICE_TYPE}${DEVICE_ID}${DEVICE_PART}1
printf ${luksKey} | cryptsetup --force-password luksOpen /dev/${DEVICE_TYPE}${DEVICE_ID}${DEVICE_PART}1 ceph-${OSD_ID}block

cat >> /etc/crypttab << EOF
ceph-${OSD_ID}block UUID=$(blkid /dev/${DEVICE_TYPE}${DEVICE_ID}${DEVICE_PART}1 | awk -F"\"" {'print $2'}) none
EOF

cat << EOF > /etc/udev/rules.d/98-dev-${DEVICE_TYPE}${DEVICE_ID}.rules
ACTION=="add|change", KERNEL=="${DEVICE_TYPE}${DEVICE_ID}", ATTR{bdi/read_ahead_kb}="8192"
ACTION=="add|change", KERNEL=="${DEVICE_TYPE}${DEVICE_ID}", ATTR{queue/max_sectors_kb}="1024"
ACTION=="add|change", KERNEL=="${DEVICE_TYPE}${DEVICE_ID}", ATTR{queue/scheduler}="deadline"
ACTION=="add|change", KERNEL=="${DEVICE_TYPE}${DEVICE_ID}", SUBSYSTEM=="block", RUN+="/bin/sh -c 'echo 1024 > /sys/block/${DEVICE_TYPE}${DEVICE_ID}/queue/nr_requests'"
EOF

cat > /etc/udev/rules.d/99-ceph-osd-${OSD_ID}.rules << EOF
ENV{DM_NAME}=="vg-ceph-${OSD_ID}" OWNER="ceph" GROUP="ceph" MODE="0660"
ENV{DM_NAME}=="vg-ceph-${OSD_ID}wal" OWNER="ceph" GROUP="ceph" MODE="0660"
ENV{DM_NAME}=="vg-ceph-${OSD_ID}db" OWNER="ceph" GROUP="ceph" MODE="0660"
ENV{DM_NAME}=="ceph-${OSD_ID}block" OWNER="ceph" GROUP="ceph" MODE="0660"
EOF

udevadm control --reload-rules; udevadm trigger
```

Initialize Ceph osd
```
chown -R ceph:ceph /var/lib/ceph/osd/ceph-${OSD_ID}
ceph-osd --setuser ceph -i ${OSD_ID} --mkkey --mkfs
ceph auth add osd.${OSD_ID} osd 'allow *' mon 'allow rwx' -i /var/lib/ceph/osd/ceph-${OSD_ID}/keyring

cat > /etc/systemd/system/podman-ceph_osd${OSD_ID}.service << EOF
[Unit]
Description=Podman container - Ceph osd ${OSD_ID}
After=network.target

[Service]
Environment=VER=v19.2.0-20240927
Type=simple
WorkingDirectory=/var/lib/ceph/osd/ceph-${OSD_ID}
TimeoutStartSec=300
ExecStartPre=-/usr/bin/podman rm -f ceph_osd${OSD_ID}
ExecStartPre=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.1${OSD_ID}
ExecStart=/usr/bin/podman run --privileged --pid=host --cpuset-cpus 0,1 --memory 2g --name ceph_osd${OSD_ID} --hostname ceph_osd${OSD_ID} --ip 172.30.0.1${OSD_ID} -v /dev:/dev -v /etc/localtime:/etc/localtime:ro -v /etc/ceph:/etc/ceph/ -v /var/lib/ceph/osd/ceph-${OSD_ID}:/var/lib/ceph/osd/ceph-${OSD_ID} -v /run/udev/:/run/udev/ -e OSD_DEVICE=/dev/mapper/ceph-${OSD_ID}block ceph/ceph:\${VER} ceph-osd --id ${OSD_ID} -c /etc/ceph/ceph.conf --cluster ceph -f
ExecStop=-/usr/bin/podman rm -f ceph_osd${OSD_ID}
ExecStopPost=-/usr/bin/rm -rf /var/lib/cni/networks/podman/172.30.0.1${OSD_ID}
Restart=always
RestartSec=10s
TimeoutStartSec=120
TimeoutStopSec=15

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload; for i in disable stop; do systemctl $i podman-ceph_osd${OSD_ID}; done
systemctl daemon-reload; for i in enable start; do systemctl $i podman-ceph_osd${OSD_ID}; done

ceph osd crush add osd.${OSD_ID} 1 host=$(hostname)
```

### Modify crush map
```
ceph osd crush move $(hostname) rack=rack1
ceph osd crush move rack1 root=default
ceph osd crush tunables optimal
```

### Create rados pools
```
ceph osd pool rm rbd rbd --yes-i-really-really-mean-it
ceph osd pool create cephfs_metadata 4
ceph osd pool create cephfs_data 256
ceph mgr module enable pg_autoscaler
```

### Create mds
```
for i in disable stop; do systemctl $i ceph-mds@bridge.service; done
for i in enable start; do systemctl $i ceph-mds@bridge.service; done
```

### Create filesystem
```
ceph fs new cephfs cephfs_metadata cephfs_data
ceph fs set cephfs standby_count_wanted 0
echo $(ceph-authtool -p /etc/ceph/keyring.client.admin) > /etc/ceph/mount.secret
mkdir /mnt/ceph
cat << EOF >> /etc/fstab
admin@.cephfs=/ /mnt/ceph ceph noauto,noatime,mon_addr=mon2:6789,secretfile=/etc/ceph/mount.secret 0 0
EOF
mount /mnt/ceph
``` 

### Create mgr instance
```
for i in disable stop; do systemctl $i ceph-mgr@$(hostname).service; done
for i in enable start; do systemctl $i ceph-mgr@$(hostname).service; done
```

## Operations

### Backup configuration
```
tar -cf /tmp/cube.tar /etc/ceph /var/lib/ceph/mon/ceph-cube
for i in $(ceph osd ls); do
  for j in ceph_fsid fsid keyring magic ready store_version superblock type whoami; do
    tar -rf /tmp/cube.tar /var/lib/ceph/osd/ceph-$i/$j
  done
done
gzip /tmp/cube.tar
openssl aes-256-cbc -a -salt -in /tmp/cube.tar.gz -out /tmp/cube.tar.gz.enc
```

### Restore configuration
```
openssl aes-256-cbc -d -a -in /tmp/cube.tar.gz.enc -out /tmp/cube.tar.gz
gunzip /tmp/cube.gz.tar.gz
cd /; tar -xf /tmp/cube.tar
```

### Start Ceph cluster

```
for i in enable start; do systemctl $i ceph-mgr@$(hostname).service; done
for i in enable start; do systemctl $i ceph-mds@bridge.service; done
for i in enable start; do systemctl $i podman-ceph_mon{1,2}; done
for OSD_ID in 0 1 2 3 4 5 6 7 8 9; do for i in enable start; do systemctl $i podman-ceph_osd${OSD_ID}; done; done
mount /mnt/ceph
ceph osd unset noout
```

#### Quorum
```
for i in enable start; do systemctl $i ceph-mon@bridge.service; done
```

### Stop Ceph cluster
```
ceph osd set noout
umount /mnt/ceph
for OSD_ID in 0 1 2 3 4 5 6 7 8 9; do for i in disable stop; do systemctl $i podman-ceph_osd${OSD_ID}; done; done
for i in disable stop; do systemctl $i podman-ceph_mon{1,2}; done
for i in disable stop; do systemctl $i ceph-mds@bridge.service; done
for i in disable stop; do systemctl $i ceph-mgr@$(hostname).service; done
```

#### Quorum
```
for i in disable stop; do systemctl $i ceph-mon@bridge.service; done
```


### Restart OSDs for config update
```
for OSD_ID in `ceph osd ls`; do for i in restart; do systemctl $i podman-ceph_osd${OSD_ID}; sleep 60; done; done
```

### Remove Ceph osd / failed disk
```
OSD_ID=
systemctl daemon-reload; for i in disable stop; do systemctl $i podman-ceph_osd${OSD_ID}; done
for i in nobackfill norebalance norecover; do ceph set $i; done
ceph osd down ${OSD_ID}
ceph osd out ${OSD_ID}
ceph osd rm ${OSD_ID}
ceph auth del osd.${OSD_ID}
ceph osd crush remove osd.${OSD_ID}
sed -i "/ceph-${OSD_ID}/d" /etc/fstab
rm -rf /var/lib/ceph/osd/ceph-${OSD_ID}/*
rm -rf /etc/udev/rules.d/99-ceph-osd-${OSD_ID}.rules
for i in '' block db wal; do sed -i "/ceph-${OSD_ID}${i}/d" /etc/crypttab; done
for i in '' block db wal; do cryptsetup luksClose ceph-${OSD_ID}${i}; done
```

- Remove phy disk

- [**Create osd instance**](https://github.com/mazzystr/Configurations/new/master#create-osd-instance)

### Disaster
```
ceph tell mds.cube damage ls | python -m "json.tool"
```
```
umount /mnt/ceph
systemctl stop ceph-mds@cube.service

cephfs-journal-tool journal export backup.bin
cephfs-journal-tool event recover_dentries summary
cephfs-journal-tool journal reset

cephfs-table-tool all reset session

ceph fs reset ceph --yes-i-really-mean-it

cephfs-table-tool 0 reset session
cephfs-table-tool 0 reset snap
cephfs-table-tool 0 reset inode
cephfs-journal-tool --rank=0 journal reset
cephfs-data-scan init --force-init

cephfs-data-scan scan_extents --worker_n 0 --worker_m 3 ceph_data
cephfs-data-scan scan_extents --worker_n 1 --worker_m 3 ceph_data
cephfs-data-scan scan_extents --worker_n 2 --worker_m 3 ceph_data
cephfs-data-scan scan_extents --worker_n 3 --worker_m 3 ceph_data

cephfs-data-scan scan_inodes --worker_n 0 --worker_m 3 ceph_data
cephfs-data-scan scan_inodes --worker_n 1 --worker_m 3 ceph_data
cephfs-data-scan scan_inodes --worker_n 2 --worker_m 3 ceph_data
cephfs-data-scan scan_inodes --worker_n 3 --worker_m 3 ceph_data

cephfs-data-scan cleanup ceph_data

systemctl reset-failed ceph-mds@cube.service; systemctl restart ceph-mds@cube.service
mount -a
```
```
umount /mnt/ceph
systemctl stop ceph-mds@cube.service
ceph mds repaired 0
```
